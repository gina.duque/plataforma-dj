<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
    <!-- style -->
    <link rel="stylesheet" href="css/style.css">
    <meta http-equiv="refresh" content="5">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <!-- Navigation -->
 <nav class="navbar  navbar-inverse navbar-static-top">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Plaza sound</a>
      </div>
  
      <!-- Collect the nav links, forms, and other content for toggling -->
      
        
        <ul class="nav navbar-nav navbar-right">
          <li><a href="#"class="btn btn-outline-danger btn-sm" id="cerrar"  onclick="Cerrar()">Salir</a></li>
          
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>

 
<div class="container">
    <div class="row">
        <div class="col-md-8">            
            <div class="col-md-12"> 
                <h2 class="textocentrado">
                    Lista
                </h2>
            </div>
            <div class="col-md-12"> 

                  
                  <table class="table">
                    <thead class="thead-light">
                      <tr>
                        
                        <th scope="col">Pista</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Link</th>
                        <th scope="col">Mesa</th>
                        <th scope="col">Reproducir</th>
                      </tr>
                    </thead>
                    <tbody id="datos">
<!-- cargo el xml y comienzo a recorrerlo -->
                    <?php
$xml = simplexml_load_file("xml/listareprodu.xml");
 
$vector=array();
$vector=$xml;
echo $xml;
$capturar=array();
foreach($vector as $data){
    

   

?>
<!-- lleno la tabla a medida que recorro el xml -->
                       <tr>
                       <td><?php echo $data->pista;   ?></td>
                       <td><?php echo $data->nombre;   ?></td>
                       <td><?php echo $data->link;   ?></td>
                       <td><?php echo $data->mesa;   ?></td>
                       <td><a href=<?php echo $data->link;?> class="btn btn btn-warning btn-sm">Play</a></td>
                       </tr>

                        <?php  }  ?>
                    </tbody>

                    
                  </table>
                  
            </div>
        </div>
        <div class="col-md-4">
            <div class="col-md-12">
                <h2 class="textocentrado">
                Mesas
                </h2>
        </div>
        <!-- llamo al archivo mesas.php -->
        <?php
require_once "mesas.php";
 
        ?>
            <div class="col-md-4"  > 
            <!-- asigno un color de fondo rojo o verde dependiendo del estado de la mesa -->           
              <div class="card  <?php echo $color1; ?>">
                <div class="card-body text-center">
                  <h1>1</h1>
                  <div class="cardtexto">
                  <!-- asigo un texto de disponible o ocupado dependiendo del estado de la mesa -->
                  <p class="card-text ">Mesa <?php echo $texto1; ?></p></div>
                </div>
              </div>  
            </div>
            <div class="col-md-4 cardmesa">
            <!-- asigno un color de fondo rojo o verde dependiendo del estado de la mesa --> 
              <div class="card <?php echo $color2; ?>">
                <div class="card-body text-center">
                  <h1>2</h1>
                  <div class="cardtexto">
                  <!-- asigo un texto de disponible o ocupado dependiendo del estado de la mesa -->
                  <p class="card-text">Mesa <?php echo $texto2; ?></p>
                </div>
                </div>
              </div>  
            </div>
            <div class="col-md-4">
              <div class="card <?php echo $color3; ?>">
            <!-- asigno un color de fondo rojo o verde dependiendo del estado de la mesa --> 
                <div class="card-body text-center">
                  <h1>3</h1>
                  <div class="cardtexto">
                  <!-- asigo un texto de disponible o ocupado dependiendo del estado de la mesa -->
                  <p class="card-text cardmesa">Mesa <?php echo $texto3; ?></p>
                </div>
                </div>
              </div>  
            </div>
            <div class="col-md-4"  >            
              <div class="card  <?php echo $color4; ?>">
            <!-- asigno un color de fondo rojo o verde dependiendo del estado de la mesa --> 
                <div class="card-body text-center">
                  <h1>4</h1>
                  <div class="cardtexto">
                  <!-- asigo un texto de disponible o ocupado dependiendo del estado de la mesa -->
                  <p class="card-text cardmesa">Mesa <?php echo $texto4; ?></p>
                </div>
                </div>
              </div>  
            </div>
            <div class="col-md-4 cardmesa">
              <div class="card <?php echo $color5; ?>">
            <!-- asigno un color de fondo rojo o verde dependiendo del estado de la mesa --> 
                <div class="card-body text-center">
                  <h1>5</h1>
                  <div class="cardtexto">
                  <p class="card-text cardmesa">Mesa <?php echo $texto5; ?></p></div>
                </div>
              </div>  
            </div>
            <div class="col-md-4">
              <div class="card <?php echo $color6; ?>">
            <!-- asigno un color de fondo rojo o verde dependiendo del estado de la mesa --> 
                <div class="card-body text-center">
                  <h1>6</h1>
                  <div class="cardtexto">
                  <!-- asigo un texto de disponible o ocupado dependiendo del estado de la mesa -->
                  <p class="card-text cardmesa">Mesa <?php echo $texto6; ?></p>
                </div>
                </div>
              </div>  
            </div>
            
        </div>
      </div>
</div>


  
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

     <script src="js/Canciones.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
  </body>
</html>