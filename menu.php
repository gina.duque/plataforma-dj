<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
    <!-- style -->
    <link rel="stylesheet" href="css/style.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <!-- Navigation -->
 <nav class="navbar  navbar-inverse navbar-static-top">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Plaza sound</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Plaza sound</a>
      </div>
  
      <!-- Collect the nav links, forms, and other content for toggling -->
      
        
        <ul class="nav navbar-nav navbar-right">
          <li><a href="#"class="btn btn-outline-danger btn-sm" id="cerrar"  onclick="Cerrar()">Salir</a></li>
          
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>


 
 
     
       
<div class="container">
    <div class="row">
        <div class="col-md-6">            
            <div class="col-md-12"> 
                <h2 class="textocentrado">
                    Lista
                </h2>
            </div>
            <div class="col-md-12"> 

                  
                  <table class="table">
                    <thead class="thead-light">
                      <tr>
                        
                        <th scope="col">Pista</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Link</th>
                        <th scope="col">Accion</th>
                      </tr>
                    </thead>
                    <tbody id="datos">

                    <?php
$xml = simplexml_load_file("xml/canciones.xml");
 
$vector=array();
$vector=$xml;
echo $xml;
$capturar=array();
foreach($vector as $data){
?>
                       <tr>
                       <td><?php echo $data->pista;   ?></td>
                       <td><?php echo $data->nombre;   ?></td>
                       <td><?php echo $data->link;   ?></td>
                     <td>  <button class="btn btn-success" data-toggle="modal" data-target="#exampleModalCenter" onclick="copiar('<?php  echo $data->pista?>','<?php  echo $data->nombre?>','<?php  echo $data->link?>  ') " >Agregar</button></td>
                       </tr>
                        <?php  }  ?>
                    </tbody>                    
                  </table>
                  
            </div>
        </div>
        <div class="col-md-6">
            <div class="col-md-12">
                <h2 class="textocentrado">
                Bienvenido
                </h2>
        </div>
        <div class="col-md-12">
        <div class="card" style="width: 100%;">
  <img class="img-fluid" src="img\menu.png" alt="Responsive image">
  <div class="card-body">
    <h4 class="card-title textocentrado">Plaza sound</h4>
    <p class="card-text textocentrado">La musica que quieres.</p>
    <a href="#" class="btn btn-danger btn-lg btn-block" id="cerrar"  onclick="Cerrar()">Salir</a>
  </div>
</div>
        </div>        
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title textocentrado" id="exampleModalLongTitle">Cancion agregada</h4>
        
      </div>
      <div class="modal-body">
        La cancion <?php echo $data->nombre;   ?> se a puesto el la cola de reproduccion
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

  
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

     <script src="js/Canciones.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
  </body>
</html>